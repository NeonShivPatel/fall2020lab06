//@author1: Dan Pomerantz
//@author2: Shiv Patel
//@ID: 1935098

import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;


public class RpsApplication extends Application {	
	private RpsGame game = new RpsGame();
	
	public void start(Stage stage) {
		Group root = new Group(); 
		
		//Creating HBox for every rps button and cretaing said Buttons
		HBox buttons = new HBox();
		Button rockButton = new Button("rock");
		Button paperButton = new Button("paper");
		Button scissorsButton = new Button("scissors");
		
		//Creating HBox for every TextField that will be displayed and creating said TextFields
		HBox textFields = new HBox();
		TextField message = new TextField("Welcome!");
		TextField winField = new TextField("wins: 0");
		TextField lossField = new TextField("losses: 0");
		TextField tieField = new TextField("ties: 0");
		
		//Adding the set on Action on all the buttons
		rockButton.setOnAction(new RpsChoice(message, winField, lossField, tieField, "rock", game));
		paperButton.setOnAction(new RpsChoice(message, winField, lossField, tieField, "paper", game));
		scissorsButton.setOnAction(new RpsChoice(message, winField, lossField, tieField, "scissors", game));
		
		//Adding the Buttons and TextFields to their respective HBox
		buttons.getChildren().addAll(rockButton, paperButton, scissorsButton);
		textFields.getChildren().addAll(message, winField, lossField, tieField);
		
		//Set the Width of the message TextField
		message.setPrefWidth(200);
		
		//Create VBox and add both of the HBox to it
		VBox vbox = new VBox();
		vbox.getChildren().addAll(buttons,textFields);
		
		//Add the VBox to the root 
		root.getChildren().add(vbox);
		
		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(Color.BLACK);

		//associate scene to stage and show
		stage.setTitle("Rock Paper Scissors"); 
		stage.setScene(scene); 
		
		stage.show(); 
	}
	
    public static void main(String[] args) {
        Application.launch(args);
    }
}    