//@author: Shiv Patel
//@ID: 1935098

import java.util.Random;

public class RpsGame {
	private int wins = 0;
	private int losses = 0;
	private int ties = 0;
	public RpsGame() {};
	
	//Get Methods for the private fields
	public int getWins() {
		return this.wins;
	}
	public int getLosses() {
		return this.losses;
	}
	public int getTies() {
		return this.ties;
	}
	
	//Method playRound will play one round based on the String Choice given and will then output a String that displays the outcome
	public String playRound(String choice) {
		Random randgen = new Random();
		String results = "";
		//Creating array containing each possible choices
		String[] choices = {"rock", "paper", "scissors"};
		int i = randgen.nextInt(3);
		
		//Various if statements for every possible outcome in a rps match and they all have their respective results
		if (choice.equalsIgnoreCase(choices[i])) {
			results = "Commputer also chose " + choices[i] + ", the game is a tie";
			ties++;
		}
		else if (choice.equalsIgnoreCase("rock") && choices[i].equalsIgnoreCase("paper")) {
			results = "Commputer plays " + choices[i] + " and the computer wins";
			losses++;
		}
		else if (choice.equalsIgnoreCase("rock") && choices[i].equalsIgnoreCase("scissors")) {
			results = "Commputer plays " + choices[i] + " and the computer loses";
			wins++;
		}
		else if (choice.equalsIgnoreCase("paper") && choices[i].equalsIgnoreCase("rock")) {
			results = "Commputer plays " + choices[i] + " and the computer loses";
			wins++;
		}
		else if (choice.equalsIgnoreCase("paper") && choices[i].equalsIgnoreCase("scissors")) {
			results = "Commputer plays " + choices[i] + " and the computer wins";
			losses++;
		}
		else if (choice.equalsIgnoreCase("scissors") && choices[i].equalsIgnoreCase("rock")) {
			results = "Commputer plays " + choices[i] + " and the computer wins";
			losses++;
		}
		else if (choice.equalsIgnoreCase("scissors") && choices[i].equalsIgnoreCase("paper")) {
			results = "Commputer plays " + choices[i] + " and the computer loses";
			wins++;
		}
		else {
			results = "There has been an erro because the give input was wrong, make sure to type either rock, paper or scissors.";
		}
		
		//return variable results
		return results;
	}
}
