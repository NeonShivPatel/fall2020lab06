import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;

public class RpsChoice implements EventHandler<ActionEvent>{
	private TextField message;
	private TextField winField;
	private TextField lossField;
	private TextField tieField;
	private String choice;
	private RpsGame game;
	
	public RpsChoice (TextField message, TextField field1, TextField field2, TextField field3, String choice, RpsGame game) {
		this.message = message;
		this.winField = field1;
		this.lossField = field2;
		this.tieField = field3;
		this.choice = choice;
		this.game = game;
	}
	
	public void handle(ActionEvent e) {
		String results = this.game.playRound(this.choice);
		this.message.setText(results);
		this.winField.setText("win: " + game.getWins());
		this.lossField.setText("losses: " + game.getLosses());
		this.tieField.setText("ties: " + game.getTies());
	}
}
